import { shallowMount } from "@vue/test-utils";
import RegistrationForm from "@/vue/components/RegistrationForm/RegistrationForm.vue";

const factory = (values = {}) => {
  return shallowMount(RegistrationForm, {
    data () {
      return {
        ...values
      }
    }
  })
}

describe("RegistrationForm.vue", () => {
  it("should submit button be disable at beginning", () => {
    const wrapper = factory();
  
    expect(wrapper.find('.submit-disable').exists()).toBeTruthy();
  });

  it("should submit button has not disable class when form is valid", () => {
    const wrapper = factory({ isFormValid: true });
  
    expect(wrapper.find('.submit-disable').exists()).toBeFalsy();
  });

  it('validate email field', async () => {
    const wrapper = factory({ 
      isFormValid: true,
      password: 'cosik'
    });

    expect(wrapper.vm.$data.passwordError).toEqual('cosik');
    // expect(wrapper.find('[data-email]').find('span').text()).toEqual('cosik');
  });
});
