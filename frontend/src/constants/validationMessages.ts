export const validationMessages: Record<string, Record<string, string>> = {
  passwordMessages: {
    smallLetter: "Powinno zawierać jedną małą litere",
    upperLetter: "Powinno zawierać jedną dużą litere",
    digital: "Powinno zawierać przynajmniej jedną cyfre",
    lengthField: "Haslo powinno byc dlugie na przynajmniej 8 znaków",
    emptyField: "Pole nie powinno być puste",
    repeated: "Hasła powinny być takie same"
  },
  emailMessages: {
    hasAtSign: "Email powinien zawierać '@'",
    incorrect: "Podany e-mail jest niepoprawny",
    emptyField: "Pole nie może zostać puste"
  }
};
