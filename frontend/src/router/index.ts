import Home from "../views/Home.vue";
import VueRouter from 'vue-router';

const routes = [
  { 
    path: "/",
    component: Home
  },
  {
    path: "/register",
    component: () =>
      import("../views/RegistrationView.vue")
  },
  {
    path: "/login",
    component: () =>
      import("../views/LoginView.vue")
  },
];

const router = new VueRouter({ mode: "history", routes });

export default router;