import { validationMessages } from "../constants/validationMessages";

export class ValidationHelper {
  public static registerPasswordValidation = (password: string): string => {
    let errorMessage = "";

    const isUpperCharacter = [...password].find(
      char => char === char.toUpperCase()
    );
    const isLowerCharacter = [...password].find(
      char => char === char.toLowerCase()
    );
    const regOnlyDigitals = new RegExp(/\d/);

    const {
      smallLetter,
      upperLetter,
      digital,
      lengthField,
      emptyField
    } = validationMessages.passwordMessages;

    if (password.length === 0) {
      errorMessage = emptyField;
      return errorMessage;
    }

    if (!isUpperCharacter) {
      errorMessage = upperLetter;
      return errorMessage;
    }

    if (!isLowerCharacter) {
      errorMessage = smallLetter;
      return errorMessage;
    }

    if (!regOnlyDigitals.test(password)) {
      errorMessage = digital;
      return errorMessage;
    }

    if (password.length < 8) {
      errorMessage = lengthField;
      return errorMessage;
    }

    return errorMessage;
  };

  public static emailValidation = (email: string): string => {
    let errorMessage = "";

    const { hasAtSign, emptyField } = validationMessages.emailMessages;

    if (email.indexOf("@") === -1) {
      errorMessage = hasAtSign;
      return errorMessage;
    }

    if (email.indexOf(".") === -1) {
      errorMessage = emptyField;
      return errorMessage;
    }

    return errorMessage;
  };

  public static registerConfirmPasswordValidation = (
    password: string,
    repeatedPassword: string
  ): string => {
    let errorMessage = "";

    errorMessage = ValidationHelper.registerPasswordValidation(
      repeatedPassword
    );

    const { repeated } = validationMessages.passwordMessages;
    if (errorMessage) {
      return errorMessage;
    }

    if (password !== repeatedPassword) {
      errorMessage = repeated;
    }

    return errorMessage;
  };

  public static registerSubmitValidation = (
    email: string,
    password: string,
    repeatedPassword: string
  ): boolean => {
    const isEmailValid = !ValidationHelper.emailValidation(email);
    const isPasswordValid = !ValidationHelper.registerPasswordValidation(
      password
    );
    const isRepeatedPasswordValid = !ValidationHelper.registerConfirmPasswordValidation(
      password,
      repeatedPassword
    );

    return isEmailValid && isPasswordValid && isRepeatedPasswordValid;
  };

  public static loginPasswordValidation = (password: string): string => {
    const { lengthField, emptyField } = validationMessages.passwordMessages;

    let errorMessage = "";

    if (password.length === 0) {
      errorMessage = emptyField;
      return errorMessage;
    }

    if (password.length < 8) {
      errorMessage = lengthField;
      return errorMessage;
    }
    return errorMessage;
  }

  public static loginSubmitValidation = (email: string, password: string): boolean => {
    const isEmailValid = !ValidationHelper.emailValidation(email);
    const isPasswordValid = !ValidationHelper.loginPasswordValidation(password);
    return isEmailValid && isPasswordValid;
  }
}
