import { UsersService } from './../users.service';
import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '../users.service';
import { UsersController } from '../users.controller';
import { User } from '../schemas/user.schema';
import { Model } from 'mongoose';
import { MESSAGES } from '../../helpers/messages/messages'

jest.mock('../../helpers/messages/messages');

describe('UsersService', () => {
  let usersService: UsersService;
  let usersController: UsersController;

  beforeEach(async () => {
    const userModel: Model<User> = new UsersService.create({ id: '1', email: 'cosik@onet.pl', password: 'kokoko' });
    usersService = new UsersService(userModel);
    usersController = new UsersController(usersService);
  });

  describe('findAll', () => {
    it('should return an array of cats', async () => {
      const users = [{ id: '123', email: 'cosik@onet.pl', password: 'koko' }, { id: '123', email: 'cosik@onet.pl', password: 'koko' }];
      jest.spyOn(usersService, 'findAll').mockImplementation(() => users as any);

      expect(await usersController.findAll()).toBe(users);
    });
  });
});
