import { Controller, Get, Post, Body, Res, HttpStatus } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './schemas/user.schema';
import { Response } from 'express';
import { MESSAGES } from '../helpers/messages/messages';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}


  @Post()
  async create(@Body() createUserDto: CreateUserDto, @Res() res: Response) {
    try {
      return await this.usersService.create(createUserDto);
    } catch {
      res.status(HttpStatus.CONFLICT).json({ 
        message: MESSAGES.register.conflict,
        status: MESSAGES.status.fail,
        code: HttpStatus.CONFLICT,
      });
    }
  }

  @Get()
  async findAll(): Promise<User[]> {
    return this.usersService.findAll();
  }
}
