import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './schemas/user.schema';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from './dto/create-user.dto';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class UsersService {

  constructor(@InjectModel(User.name) public userModel: Model<User>) {
  }

  async findOne(email: string): Promise<User> {
    return this.userModel.findOne({ email }).exec();
  }

  async findAll(): Promise<User[]> {
    return await this.userModel.find().exec();
  }

  async create(createUserDto: CreateUserDto): Promise<any> {
    const emailAlreadyTaken = await this.findOne(createUserDto.email);

    if (!!emailAlreadyTaken) {
      throw new Error('Email already taken!');
    }

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(createUserDto.password, salt);

    const userId = uuidv4();
    const createUser = new this.userModel({ email: createUserDto.email, password: hashedPassword, id: userId });

    return createUser.save();
  }
}
