import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { User } from 'src/users/schemas/user.schema';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findOne(email);

    if (user) {
      const { password, ...result } = user;
      return result;
    }

    return null;
  }

  async register(createUserDto: CreateUserDto): Promise<User> {
    return await this.usersService.create(createUserDto);
  }

  async login(userLoginData: any): Promise<any> {
    const user = await this.usersService.findOne(userLoginData.email);
    const payload  = { email: user.email, sub: user.id };

    return {
      access_token: this.jwtService.sign(payload),
    }
  }
}
