import { Controller, Get, Post, Body, Res, Request, HttpStatus, UseGuards } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { User } from '../users/schemas/user.schema';
import { Response } from 'express';
import { MESSAGES } from 'src/helpers/messages/messages';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly usersService: UsersService, private readonly authService: AuthService) {}


  @Post('/register')
  async create(@Request() req, @Res() res: Response) {
    try {
      await this.authService.register(req.body);
      res.status(HttpStatus.CREATED).send();
    } catch {
      res.status(HttpStatus.CONFLICT).json({ 
        message: MESSAGES.register.conflict,
        status: MESSAGES.status.fail,
        code: HttpStatus.CONFLICT,
      });
    }
  }

  @UseGuards(LocalAuthGuard)
  @Post('/login')
  async login(@Request() req) {
    try {
      return this.authService.login(req.user);
    } catch (e) {
      console.log(e, 'error z authService');
    }
  }


  @Get()
  async findAll(): Promise<User[]> {
    return this.usersService.findAll();
  }
}
