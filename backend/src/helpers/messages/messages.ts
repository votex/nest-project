export const MESSAGES: Record<string, Record<string, string>> = {
  status: { 
    success: 'success',
    fail: 'failure'
  },
  register: {
    conflict: 'Email already taken :('
  }
}
